# c4-echo-lambda
This repository contains an example of how to integrate the Amazon Echo with a Control4 controller. The scope of the demo includes turning on and off lights in a given room. The code requires you to provide a valid JWT for invoking the Broker REST API. You can generate one of those with the toy page on your controller `https://[controller ip address]/api/v1/jwt/html`.

## Getting Started
This integration makes use of several Amazon services, so you must have an Amazon developer account. Be aware that the services required for this to work are not included in the free processing tier. So don't get carried away unless you want a huge bill from Amazon at the end of the month.

The Amazon services required to implement this solution are as follows. The authoritative source for Amazon Echo development can be found at [Amazon Developers, Meet Alexa](https://developer.amazon.com/appsandservices/solutions/alexa). 

 - **_Amazon Lambda_** used to implement an event responder that receives requests from the Echo 
 - **_Amazon Alexa Skills Kit_** used to define a grammar, application and skill triggers, etc.

Note that it is also possible to host your own service that responds to input received from the Echo.

### Defining a Lambda Function
A lambda function is a block of code hosted in AWS that is capable of responding to certain events, such as when a consumer asks their Echo to do something.  Fortunately, there is a Lambda function blueprint specifically geared towards creating a responder for Alexa interactions.  The sample code in this repo was based on this blueprint.  For the sake of completeness, this section will describe the full procedure for creating a lambda function for receiving Alexa input.

1. Visit the Amazon Lambda page from aws.amazon.com.
2. Click the button to create  a new Lambda function.
3. Select the `alexa-skills-kit-color-expert` blueprint
4. Ensure that the Event source type field shows `Alexa Skills Kit`
5. Give your function a name and description.
6. Ensure that `Node.js` is selected as the runtime
7. `npm install` for this repository
8. Zip up the `index.js`, `package.json` files and the `node_modules` folder in the root of this repository and upload that zip file.
9. Select an execution role for the lambda function (I recommend choosing the basic execution role if you don't want to go to the trouble of creating a custom execution role)
10. Leave the memory as is but increase the timeout value to 5 seconds
11. On the Review page, make sure that the event source is still `Alexa`, then click `Create function`

Once your lambda function has been created, you should end up on a detail page for your lambda function.  Make a note of the ARN shown on the top-right corner of the screen (copy it to the clipboard, you're going to need it in a minute).

See also: [Developing an Alexa Skill as a Lambda Function](https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/developing-an-alexa-skill-as-a-lambda-function)

### Creating an Alexa Skill
An Alexa skill definition consists of a declaration of what types of commands are supported by your skill, a representation of the grammar that might be used when speaking these commands, and the specificaiton of an endpoint (your lambda function) for handling the input.  This section will review a few of these key concepts before describing the procedure for creating an Alexa Skill. 

#### Defining the Intent Schema
An intent schema is a JSON file that is used to describe the types of commands that a consumer can issue to the Echo. The intent schema also defines "slots" or parameters to each command. An intent schema that declares a single intent to turn on the lights in a room would look something like the following.

```
{
  "intents": [{
    "intent": "TurnOnLightsInRoom",
    "slots": [{
      "name": "roomName",
      "type": "LITERAL"
    }]
  }]
}
```

Intent names must not include spaces or punctuation.

#### Defining an Utterances File
The utterances file is used to teach the Alexa skills service how to recognize what the consumer is asking it to do. Utterances are defined in a flat text file where each line begins with the name of an intent, such as `TurnOnLightsInRoom`, followed by the phrase that represents how a consumer might interact with this Alexa skill. Consider the following sample utterances file.

```
TurnOnLightsInRoom turn on the lights in the {family room|roomName}
TurnOnLightsInRoom turn on the {family room|roomName}
TurnOnLightsInRoom turn on the {family room|roomName} lights
TurnOnLightsInRoom turn up the lights in the {family room|roomName}
TurnOnLightsInRoom turn up the {family room|roomName}
TurnOnLightsInRoom turn up the {family room|roomName} lights
TurnOnLightsInRoom light up the {family room|roomName}
```

It may take quite some time to produce an utterances file that covers all the common ways that the consumer might interact with a skill. Within each utterance you will see sections wrapped in braces, such as {family room|roomName}. This represents a slot, where the Echo service will attempt to recognize the text spoken as the value of a "slot" or variable. You can see from the example above that this requires the specification of the text the user might speak.

This assumed foreknowledge of the complete set of possible domain values for a given slot represents a problem where Control4 projects may have rooms named like "John's Bedroom". Also consider the variations of more common room names as they might be recorded in a Control4 project. The example below illustrates how to address the problem where "Family Room" might be represented by that name in a Control4 project, or simply "Family".

```
TurnOnLightsInRoom turn on the lights in the {family room|roomName}
TurnOnLightsInRoom turn on the lights in the {family|roomName} room
TurnOnLightsInRoom turn on the {family room|roomName}
TurnOnLightsInRoom turn on the {family|roomName} room
TurnOnLightsInRoom turn on the {family room|roomName} lights
TurnOnLightsInRoom turn on the {family|roomName} lights room
TurnOnLightsInRoom turn up the lights in the {family room|roomName}
TurnOnLightsInRoom turn up the lights in the {family|roomName} room
TurnOnLightsInRoom turn up the {family room|roomName}
TurnOnLightsInRoom turn up the {family|roomName} room
TurnOnLightsInRoom turn up the {family room|roomName} lights
TurnOnLightsInRoom turn up the {family|roomName} lights room
TurnOnLightsInRoom light up the {family room|roomName}
TurnOnLightsInRoom light up the {family|roomName} room
```

As you can see the specificity required in the grammar makes it quite difficult to produce a single grammar that could effectively cover all possible configurations of a Control4 project. For this reason, **_it might be appropriate to consider this capability as a value-add, paid subscription offering._** This would allow us to create specific grammars based on the known configuration of the consumer's Control4 project.

#### Selecting an Invocation Name
Note that by themselves the phrases defined in the utterances file cannot trigger the expected behavior in the Echo.  You must also select an ***invocation name***, which is a spoken word or phrase that is used to help the Echo services identify your particular Alexa skill.  When interacting with the Echo a consumer will typically speak a phrase in the following form.

```
Alexa, ask [invocation name] to [utterance]
```
or
```
Alexa, tell [invocation name] to [utterance]
```
so a complete phrase might look something like this:
```
Alexa, ask my home to turn on the lights in the family room
```
Quite a mouthful!  Apparently, closer integration with Alexa is possible through partnership with Amazon, since it is possible to interact with WeMo, Wink and SmartThings devices with phrases as simple as:
```
Alexa, turn on the office lights
```


See also: [Defining the Voice Interface](https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/defining-the-voice-interface)

#### Putting It All Together
Visit the [Alexa Skills](https://developer.amazon.com/edw/home.html#/skills) developer page, and click the `Add a New Skill` button.

1. Give the skill a name
2. Enter an invocation name.  This is a simple phrase that will help the Echo identify that it is your skill that should be invoked.  For example, you could use 'my home' or 'control4'
3. Give your skill a version number.
4. Select the Lambda ARN radio button
5. Paste the ARN for your lambda function in the Endpoint field
6. Hit Save
7. On the Interaction Model tab, enter the intent schema and sample utterances
8. Hit Next
9. Make sure your skill is enabled on the Test tab

### Testing your Skill
Install the Echo app on your Android or iOS device.  Setup your Echo device according to the instructions given.  Then, speak the following phrase:

```
Alexa, tell my home to turn on the lights in the family room
```

If all goes, well you should see those lights turn on.  

### Troubleshooting

**"I'm sorry, I'm having trouble connecting to your \[app name\] right now."**

*Your lambda function crashed.  Check the logs.*

**The echo appears to be working then gives a short bloop and goes dark**

*Your lambda function likely timed out.  I had to increase the timeout to 5 seconds.  YMMV*


