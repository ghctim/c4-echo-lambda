var request = require('request-promise');
var url = require('url');
var title = require('to-title-case');
var Promise = require('promise');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var jwt = 'eyJhbGciOiJSUzI1NiJ9.eyJTeXN0ZW1UaW1lIjoiMTQzNjE2NDMxODIyMyIsIkNvbW1vbk5hbWUiOiJjb250cm9sNF9oYzgwMF8wMDBGRkY1N0I4RTkiLCJTZXJ2aWNlcyI6ImRpcmVjdG9yLHN5c21hbiIsIlJlYWxtIjoic2lwLWJldGEyLmNvbnRyb2w0LmNvbTo1MDgwIiwiVXNlck5hbWUiOiJsZXJpY2tzb24tZGVhbGVyX2JldGEyQGNvbnRyb2w0LmNvbSIsIlBlcm1pc3Npb25zIjoiLCAvc3lzbWFuLCAvbWFuYWdlbGljZW5zZXMsIC9zdG9yYWdlLCAvZGlyZWN0b3IsIC9hdXRoL3VzZXIsIC9yZW1vdGUvc3lzbWFuLCAvcmVtb3RlL2RpcmVjdG9yIiwiaWF0IjoxNDM2MTY0MzE4LCJleHAiOjE0MzYyNTA3MTh9.RCHTt86XvqxIyudCQ8fcm2CX8NnWVeliliTR1jFFK4yXwy6lMJPgbhKTUREWZDGwDkWe-T8ZnXOPdbQqJ3PG6rxS-e6ispdBplOdsQTiyn6C_F-b0GGECP6TRcWohzMRnf-941eItlYuRmFsSEkMRWAMbtgKtR4INLu1v3SPM3F6e0aWXRYJCnKG-BDYdi70NBknLdKBG_7V4YRmNgdm0d6omYoFJKJZJqJBzgTtJd6DXB11K5P3zfpkjUOlr0IvyJI9Zl2L37fkGePKcyhld6nfkxA9VUdPXQTtwtUA30UFP8wvHqjBEUZvtgbliaj2NIj1tmMCpz4wtbH3nE55Hg';

/**
 * This sample demonstrates a simple skill built with the Amazon Alexa Skills Kit.
 * For additional samples, visit the Alexa Skills Kit developer documentation at
 * https://developer.amazon.com/appsandservices/solutions/alexa/alexa-skills-kit/getting-started-guide
 */

// Route the incoming request based on type (LaunchRequest, IntentRequest,
// etc.) The JSON body of the request is provided in the event parameter.
exports.handler = function (event, context) {
    try {
        
        console.log('hander.event: '+JSON.stringify(event));
        console.log('hander.context: '+JSON.stringify(context));
        
        console.log("C4RESTAPI event.session.application.applicationId=" + event.session.application.applicationId);

        /**
         * Uncomment this if statement and populate with your skill's application ID to
         * prevent someone else from configuring a skill that sends requests to this function.
         */
        if (event.session.application.applicationId !== "amzn1.echo-sdk-ams.app.920c976c-5a02-410c-a1f8-feed816110d7") {
            context.fail("Invalid Application ID");
        }

        if (event.session.new) {
            onSessionStarted({ requestId: event.request.requestId }, event.session);
        }

        if (event.request.type === "LaunchRequest") {
            onLaunch(event.request,
                event.session,
                function callback(sessionAttributes, speechletResponse) {
                    context.succeed(buildResponse(sessionAttributes, speechletResponse));
                });
        } else if (event.request.type === "IntentRequest") {
            onIntent(event.request,
                event.session,
                function callback(sessionAttributes, speechletResponse) {
                    context.succeed(buildResponse(sessionAttributes, speechletResponse));
                });
        } else if (event.request.type === "SessionEndedRequest") {
            onSessionEnded(event.request, event.session);
            context.succeed();
        }
    } catch (e) {
        context.fail("Exception: " + e);
    }
};

/**
 * Called when the session starts.
 */
function onSessionStarted(sessionStartedRequest, session) {
    console.log("C4RESTAPI onSessionStarted requestId=" + sessionStartedRequest.requestId
        + ", sessionId=" + session.sessionId);
}

/**
 * Called when the user launches the skill without specifying what they want.
 */
function onLaunch(launchRequest, session, callback) {
    console.log("C4RESTAPI onLaunch requestId=" + launchRequest.requestId
        + ", sessionId=" + session.sessionId);

    // Dispatch to your skill's launch.
    getWelcomeResponse(callback);
}

/**
 * Called when the user specifies an intent for this skill.
 */
function onIntent(intentRequest, session, callback) {
    console.log("C4RESTAPI onIntent requestId=" + intentRequest.requestId
        + ", sessionId=" + session.sessionId);

    var intent = intentRequest.intent,
        intentName = intentRequest.intent.name;

    console.log('C4RESTAPI intentName=' + intentName);

    // Dispatch to your skill's intent handlers
    if ("TurnOffLightsInRoom" === intentName) {
        turnOffAllLightsInRoom(intent, session, callback);
    } else if ("TurnOnLightsInRoom" === intentName) {
        turnOnAllLightsInRoom(intent, session, callback);
        // } else if ("TurnOffAllLightsOnFloor" === intentName) {
        //     turnOffAllLightsOnFloor(intent, session, callback);
        // } else if ("TurnOnAllLightsOnFloor" === intentName) {
        //     turnOnAllLightsOnFloor(intent, session, callback);
        // } else if ("TurnOffAllLights" === intentName) {
        //     turnOffAllLights(intent, session, callback);
        // } else if ("TurnOnAllLights" === intentName) {
        //     turnOnAllLights(intent, session, callback);
    } else {
        getWelcomeResponse(callback);
    }
}

function turnOnAllLightsInRoom(intent, session, callback) {
    var command = { command: 'ON' };
    var speechOutput;
    var sessionAttributes = {};
    var shouldEndSession = false;
    var roomName;
    var repromptText;

    console.log('C4RESTAPI intent slots are: ' + JSON.stringify(intent.slots));

    if (intent.slots && intent.slots.roomName) {
        roomName = intent.slots.roomName.value;
    }

    console.log('C4RESTAPI roomName = ' + roomName);

    if (roomName) {
        getLightsInRoomByName(roomName).then(
            function (result) {
                // console.log('C4RESTAPI proxies in room returned = ' + JSON.stringify(result));
                var items = JSON.parse(result);
                console.log('C4RESTAPI proxies in room count = ' + items.length);
                if (items.length === 0) {
                    console.log('no lights in the given room');
                    repromptText = 'I\'m sorry, I was not able to find any lights in a room called ' + roomName + ' in your home.';
                    callback(sessionAttributes,
                        buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
                } else {
                    console.log('making command requests');
                    var requests = [];
                    for (var i in items) {
                        var proxy = items[i];
                        var p = sendLightCommand(proxy, command);
                        requests.push(p);
                    }

                    // console.log('C4RESTAPI lights on command send to all devices, ending session');
                    // speechOutput = 'Okay, i\'m turning on the lights in the ' + roomName;
                    // shouldEndSession = true;
                    // callback(sessionAttributes,
                    //     buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));


                    console.log('waiting for all command requests');
                    Promise.all(requests).then(
                        function (result) {
                            console.log('all command requests completed');
                            speechOutput = 'Okay, i\'ve turned on the ' + roomName + ' lights';
                            shouldEndSession = true;
                            // Setting repromptText to null signifies that we do not want to reprompt the user.
                            // If the user does not respond or says something that is not understood, the session
                            // will end.
                            console.log('C4RESTAPI lights on command send to all devices, ending session');
                            callback(sessionAttributes,
                                buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
                        },
                        function (err) {
                            console.log('error waiting for all command requests');
                            repromptText = 'I\'m sorry, I was not able to complete your request.';
                            callback(sessionAttributes,
                                buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
                        });
                }
            },
            function (err) {
                console.log('error getting proxies in room');
                repromptText = 'I\'m sorry, I was not able to find any devices of that kind in a room called ' + roomName + ' in your home.';
                callback(sessionAttributes,
                    buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
            });
    } else {
        console.log('room name not given');
        speechOutput = "I didn't understand which room you want me to turn the lights on in.";
        callback(sessionAttributes,
            buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
    }
}

function turnOffAllLightsInRoom(intent, session, callback) {
    var command = { command: 'OFF' };
    var speechOutput;
    var sessionAttributes = {};
    var shouldEndSession = false;
    var roomName;
    var repromptText;

    console.log('C4RESTAPI intent slots are: ' + JSON.stringify(intent.slots));

    if (intent.slots && intent.slots.roomName) {
        roomName = intent.slots.roomName.value;
    }

    if (roomName) {
        getLightsInRoomByName(roomName).then(
            function (result) {
                var items = JSON.parse(result);
                console.log('C4RESTAPI proxies in room count = ' + items.length);
                if (items.length === 0) {
                    repromptText = 'I\'m sorry, I was not able to find any lights in a room called ' + roomName + ' in your home.';
                    callback(sessionAttributes,
                        buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
                } else {
                    var requests = [];
                    for (var i in items) {
                        var proxy = items[i];
                        var p = sendLightCommand(proxy, command);
                        requests.push(p);
                    }

                    // speechOutput = 'Okay, i\'m turning off the lights in the ' + roomName;
                    // shouldEndSession = true;
                    // callback(sessionAttributes,
                    //     buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));

                    Promise.all(requests).then(
                        function () {
                            console.log('C4RESTAPI all devices processed, ending session');
                            speechOutput = 'Okay, i\'ve turned off the ' + roomName + ' lights';
                            shouldEndSession = true;
                            // Setting repromptText to null signifies that we do not want to reprompt the user.
                            // If the user does not respond or says something that is not understood, the session
                            // will end.
                            callback(sessionAttributes,
                                buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
                        },
                        function () {
                            repromptText = 'I\'m sorry, I was not able to completely fulfill your request.';
                            callback(sessionAttributes,
                                buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
                        });
                }
            },
            function (err) {
                repromptText = 'I\'m sorry, I was not able to find a room called ' + roomName + ' in your home.';
                callback(sessionAttributes,
                    buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
            });
    } else {
        repromptText = "I didn't understand which room you want me to turn the lights off in.";
        callback(sessionAttributes,
            buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
    }
}

// function turnOnAllLightsOnFloor(intent, session, callback) {
//     var command = { command: 'ON' };
//     var speechOutput;
//     var sessionAttributes = {};
//     var shouldEndSession = false;
//     var floorName;
//     var repromptText;

//     console.log('C4RESTAPI intent slots are: ' + JSON.stringify(intent.slots));

//     if (intent.slots && intent.slots.floorName) {
//         floorName = intent.slots.floorName.value;
//     }

//     if (floorName) {
//         getProxiesOnFloorByName(floorName, ['light_v2', 'light']).then(
//             function (result) {
//                 console.log('C4RESTAPI proxies in room returned = ' + JSON.stringify(result));
//                 if (result.length === 0) {
//                     repromptText = 'I\'m sorry, I was not able to find any lights on the ' + floorName + ' in your home.';
//                     callback(sessionAttributes,
//                         buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//                 } else {
//                     var requests = [];
//                     for (var i in result) {
//                         var proxy = result[i];
//                         var p = sendCommandToDevice(proxy, command);
//                         requests.push(p);
//                     }
                    
//                     Promise.all(requests).then(
//                         function(result) {
//                             console.log('C4RESTAPI all devices processed, ending session');
//                             speechOutput = 'Okay, i\'ve turned on all the lights on the ' + floorName;
//                             shouldEndSession = true;
//                             // Setting repromptText to null signifies that we do not want to reprompt the user.
//                             // If the user does not respond or says something that is not understood, the session
//                             // will end.
//                             callback(sessionAttributes,
//                                 buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//                         },
//                         function(err) {
//                             repromptText = 'I\'m sorry, I was not able to completely fulfill your request.';
//                             callback(sessionAttributes,
//                                 buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//                         });
//                 }
//             },
//             function (err) {
//                 repromptText = 'I\'m sorry, I was not able to find a floor called ' + floorName + ' in your home.';
//                 callback(sessionAttributes,
//                     buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//             });
//     } else {
//         repromptText = "I'm not sure I understood which room you want me to turn the lights off in.";
//         callback(sessionAttributes,
//             buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//     }
// }

// function turnOffAllLightsOnFloor(intent, session, callback) {
//     var command = { command: 'OFF' };
//     var speechOutput;
//     var sessionAttributes = {};
//     var shouldEndSession = false;
//     var floorName;
//     var repromptText;

//     console.log('C4RESTAPI intent slots are: ' + JSON.stringify(intent.slots));

//     if (intent.slots && intent.slots.floorName) {
//         floorName = intent.slots.floorName.value;
//     }

//     if (floorName) {
//         getProxiesOnFloorByName(floorName, ['light_v2', 'light']).then(
//             function (result) {
//                 console.log('C4RESTAPI proxies in room returned = ' + JSON.stringify(result));
//                 if (result.length === 0) {
//                     repromptText = 'I\'m sorry, I was not able to find any lights on the ' + floorName + ' in your home.';
//                     callback(sessionAttributes,
//                         buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//                 } else {
//                     var requests = [];
//                     for (var i in result) {
//                         var proxy = result[i];
//                         var p = sendCommandToDevice(proxy, command);
//                         requests.push(p);
//                     }
                    
//                     Promise.all(requests).then(
//                         function(result) {
//                             console.log('C4RESTAPI all devices processed, ending session');
//                             speechOutput = 'Okay, i\'ve turned on all the lights on the ' + floorName;
//                             shouldEndSession = true;
//                             // Setting repromptText to null signifies that we do not want to reprompt the user.
//                             // If the user does not respond or says something that is not understood, the session
//                             // will end.
//                             callback(sessionAttributes,
//                                 buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));                            
//                         },
//                         function(err) {
//                             repromptText = 'I\'m sorry, I was not able to completely fulfill your request.';
//                             callback(sessionAttributes,
//                                 buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//                         });
//                 }
//             },
//             function (err) {
//                 repromptText = 'I\'m sorry, I was not able to find a floor called ' + floorName + ' in your home.';
//                 callback(sessionAttributes,
//                     buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//             });
//     } else {
//         repromptText = "I'm not sure I understood which room you want me to turn the lights off in.";
//         callback(sessionAttributes,
//             buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//     }
// }

// function turnOffAllLights(intent, session, callback) {
//     var command = { command: 'OFF' };
//     var speechOutput;
//     var sessionAttributes = {};
//     var shouldEndSession = false;
//     var floorName;
//     var repromptText;

//     console.log('C4RESTAPI intent slots are: ' + JSON.stringify(intent.slots));

//     if (intent.slots && intent.slots.floorName) {
//         floorName = intent.slots.floorName.value;
//     }

//     if (floorName) {
//         getProxiesByName(['light_v2', 'light']).then(
//             function (result) {
//                 console.log('C4RESTAPI proxies in room returned = ' + JSON.stringify(result));
//                 if (result.length === 0) {
//                     repromptText = 'I\'m sorry, I was not able to find any lights on the ' + floorName + ' in your home.';
//                     callback(sessionAttributes,
//                         buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//                 } else {
//                     var requests = [];
//                     for (var i in result) {
//                         var proxy = result[i];
//                         var p = sendCommandToDevice(proxy, command);
//                         requests.push(p);
//                     }
                    
//                     Promise.all(requests).then(
//                         function(result) {
//                             console.log('C4RESTAPI all devices processed, ending session');
//                             speechOutput = 'Okay, i\'ve turned on all the lights on the ' + floorName;
//                             shouldEndSession = true;
//                             // Setting repromptText to null signifies that we do not want to reprompt the user.
//                             // If the user does not respond or says something that is not understood, the session
//                             // will end.
//                             callback(sessionAttributes,
//                                 buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//                         },
//                         function(err) {
//                             repromptText = 'I\'m sorry, I was not able to completely fulfill your request.';
//                             callback(sessionAttributes,
//                                 buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//                         });
//                 }
//             },
//             function (err) {
//                 repromptText = 'I\'m sorry, I was not able to find a floor called ' + floorName + ' in your home.';
//                 callback(sessionAttributes,
//                     buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//             });
//     } else {
//         repromptText = "I'm not sure I understood which room you want me to turn the lights off in.";
//         callback(sessionAttributes,
//             buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//     }
// }

// function turnOnAllLights(intent, session, callback) {
//     var command = { command: 'ON' };
//     var speechOutput;
//     var sessionAttributes = {};
//     var shouldEndSession = false;
//     var floorName;
//     var repromptText;

//     console.log('C4RESTAPI intent slots are: ' + JSON.stringify(intent.slots));

//     if (intent.slots && intent.slots.floorName) {
//         floorName = intent.slots.floorName.value;
//     }

//     if (floorName) {
//         getProxiesByName(['light_v2', 'light']).then(
//             function (result) {
//                 console.log('C4RESTAPI proxies in room returned = ' + JSON.stringify(result));
//                 if (result.length === 0) {
//                     repromptText = 'I\'m sorry, I was not able to find any lights on the ' + floorName + ' in your home.';
//                     callback(sessionAttributes,
//                         buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//                 } else {
//                     var requests = [];
//                     for (var i in result) {
//                         var proxy = result[i];
//                         var p = sendCommandToDevice(proxy, command);
//                         requests.push(p);
//                     }
                    
//                     Promise.all(requests).then(
//                         function(result) {
//                             console.log('C4RESTAPI all devices processed, ending session');
//                             speechOutput = 'Okay, i\'ve turned on all the lights on the ' + floorName;
//                             shouldEndSession = true;
//                             // Setting repromptText to null signifies that we do not want to reprompt the user.
//                             // If the user does not respond or says something that is not understood, the session
//                             // will end.
//                             callback(sessionAttributes,
//                                 buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//                         },
//                         function(err) {
//                             repromptText = 'I\'m sorry, I was not able to completely fulfill your request.';
//                             callback(sessionAttributes,
//                                 buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//                         });
//                 }
//             },
//             function (err) {
//                 repromptText = 'I\'m sorry, I was not able to find a floor called ' + floorName + ' in your home.';
//                 callback(sessionAttributes,
//                     buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//             });
//     } else {
//         repromptText = "I'm not sure I understood which room you want me to turn the lights off in.";
//         callback(sessionAttributes,
//             buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
//     }
// }

function sendLightCommand(device, command) {

    return new Promise(function (resolve, reject) {
        console.log('C4RESTAPI sending command ' + JSON.stringify(command) + ' to device ' + device.name + ' (' + device.id + '), type=' + device.type);

        var url = 'https://reflector-beta2.control4.com/api/v1/categories/lights/' + device.id + '/commands?JWT=' + jwt;
        console.log('C4RESTAPI: command url: ' + url);

        sendCommand(url, command).then(
            function (result) {
                console.log('C4RESTAPI success response from sending command: '+JSON.stringify(result));
                resolve(result);
            },
            function (err) {
                console.log('C4RESTAPI error response from sending command: '+JSON.stringify(err));
                reject(err);
            });
    });
}

function getLightsInRoomByName(roomName) {
    console.log('getting lights in room: ' + title(roomName));
    var query = { roomName: title(roomName), type: 7 };
    var address = 'https://reflector-beta2.control4.com/api/v1/categories/lights?q=' + encodeURIComponent(JSON.stringify(query)) + '&JWT=' + jwt;

    console.log('get lights in room url: ' + address);
    var options = {
        url: url.parse(address),
        // json: true,
        rejectUnauthorized: false,
        requestCert: true,
        agent: false
    };

    return request.get(options);
}

function sendCommand(address, command) {

    console.log('C4RESTAPI sending command: ' + JSON.stringify(command));

    var options = {
        url: url.parse(address),
        json: true,
        body: command,
        rejectUnauthorized: false,
        requestCert: true,
        agent: false
    };

    return request.post(options);
}

// function getProxiesByName(proxyNames) {
//     var query = {
//         $or: []
//     };

//     for (var i in proxyNames) {
//         query.$or.push({ proxy: proxyNames[i] });
//     }

//     console.log('C4RESTAPI proxies by name query =' + JSON.stringify(query));

//     var address = 'https://reflector-beta2.control4.com/api/v1/items?JWT=' + jwt
//         + '&query=' + encodeURIComponent(JSON.stringify(query));

//     var options = {
//         method: 'GET',
//         url: url.parse(address),
//         json: true,
//         rejectUnauthorized: false,
//         requestCert: true,
//         agent: false
//     };

//     return request.get(options);
// }

// function getProxiesOnFloorByName(name, proxyNames) {

//     var query = {
//         floorName: title(name),
//         $or: []
//     };

//     for (var i in proxyNames) {
//         query.$or.push({ proxy: proxyNames[i] });
//     }

//     console.log('C4RESTAPI proxies on floor query =' + JSON.stringify(query));

//     var address = 'https://reflector-beta2.control4.com/api/v1/items?JWT=' + jwt
//         + '&query=' + encodeURIComponent(JSON.stringify(query));

//     var options = {
//         method: 'GET',
//         url: url.parse(address),
//         json: true,
//         rejectUnauthorized: false,
//         requestCert: true,
//         agent: false
//     };

//     return request.get(options);
// }

// function getProxiesInRoomByName(name, proxyNames) {

//     var query = {
//         roomName: title(name),
//         $or: []
//     };

//     for (var i in proxyNames) {
//         query.$or.push({ proxy: proxyNames[i] });
//     }

//     console.log('C4RESTAPI proxies in room query =' + JSON.stringify(query));

//     var address = 'https://reflector-beta2.control4.com/api/v1/items?JWT=' + jwt
//         + '&query=' + encodeURIComponent(JSON.stringify(query));

//     var options = {
//         method: 'GET',
//         url: url.parse(address),
//         json: true,
//         rejectUnauthorized: false,
//         requestCert: true,
//         agent: false
//     };

//     return request.get(options);
// }

/**
 * Called when the user ends the session.
 * Is not called when the skill returns shouldEndSession=true.
 */
function onSessionEnded(sessionEndedRequest, session) {
    console.log("C4RESTAPI onSessionEnded requestId=" + sessionEndedRequest.requestId
        + ", sessionId=" + session.sessionId);
    // Add cleanup logic here
}

// --------------- Functions that control the skill's behavior -----------------------

function getWelcomeResponse(callback) {
    // If we wanted to initialize the session to have some attributes we could add those here.
    var sessionAttributes = {};
    var cardTitle = "Welcome";
    var speechOutput = "Welcome to the Control4 Connected Home voice control interface, "
        + "You can say things like, "
        + "turn off the family room lights, "
        + 'or, '
        + 'turn on all the lights on the first floor.';
    // If the user either does not reply to the welcome message or says something that is not
    // understood, they will be prompted again with this text.
    var repromptText = "Try telling me to turn on or off the lights in a room in your home";
    var shouldEndSession = false;

    callback(sessionAttributes,
        buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
}

// --------------- Helpers that build all of the responses -----------------------

function buildSpeechletResponse(title, output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "PlainText",
            text: output
        },
        card: {
            type: "Simple",
            title: "SessionSpeechlet - " + title,
            content: "SessionSpeechlet - " + output
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    }
}

function buildResponse(sessionAttributes, speechletResponse) {
    return {
        version: "1.0",
        sessionAttributes: sessionAttributes,
        response: speechletResponse
    }
}