
var event = {
    "version": "1.0",
    "session": {
        "new": true,
        "sessionId": "amzn1.echo-api.session.54f6a8d3-9f8b-43c1-881b-8948c6fc52ca",
        "application": {
            "applicationId": "amzn1.echo-sdk-ams.app.920c976c-5a02-410c-a1f8-feed816110d7"
        },
        "user": {
            "userId": "amzn1.account.AHR5L3CF2VDJVXQ3LJUSB76NROBQ"
        }
    },
    "request": {
        "type": "IntentRequest",
        "requestId": "amzn1.echo-api.request.a1c94688-1f23-48da-a734-79d97ef1263e",
        "timestamp": "2015-07-06T07:17:14Z",
        "intent": {
            "name": "TurnOnLightsInRoom",
            "slots": {
                "roomName": {
                    "name": "roomName",
                    "value": "dining room"
                }
            }
        }
    }
};

var context = {
    "awsRequestId": "06b9d8ca-23af-11e5-be97-af20457db424",
    "invokeid": "06b9d8ca-23af-11e5-be97-af20457db424",
    "logGroupName": "/aws/lambda/Control4MyHome",
    "logStreamName": "2015/07/06/e6dff147718d4d3aa130840e264ab720",
    "functionName": "Control4MyHome",
    "memoryLimitInMB": "128"
};

var handler = require('..').handler;

handler(event, context);
